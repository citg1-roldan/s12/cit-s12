package com.zuitt.discussion.services;

import com.zuitt.discussion.models.User;
import org.springframework.http.ResponseEntity;



public interface UserService {
    //    Create a User
    void createUser(User user);
    //    Viewing all posts
    Iterable<User> getUsers();
    //    Delete a User
    ResponseEntity deleteUser(Long id);

    //    Update a User
    ResponseEntity updateUser(Long id, User user);
}
